package chapter06;

import java.util.Random;

/**
This class models a die that, when cast, lands on a random
face.
*/
public class Die {
	
	private int sides;
	private Random generator;
	
	public Die(int s){
		sides = s; 
		generator = new Random();
	}
	public int cast(){
		return 1 + generator.nextInt(sides);
	}
}

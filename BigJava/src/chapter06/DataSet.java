package chapter06;

/**
Computes information about a set of data values.
*/
public class DataSet {
	
	private double sum;
	private double maximum;
	private int count;
	
	public DataSet(){
		sum = 0;
		maximum = 0;
		count = 0;
	}
	public void add(double x){
		sum = sum +x ;
		if (count == 0||maximum < x) 
			maximum = x;
		count++;
	}
	public double getAverage(){
		double avg = 0;
		if (count != 0) 
			avg = sum / count; 
		return avg;
	}
	public double getMaximum(){
		return maximum;
	}
}

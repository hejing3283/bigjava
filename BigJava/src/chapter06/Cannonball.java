package chapter06;
/**
This class simulates a cannonball fired up in the air.
*/
public class Cannonball {
	final static double DELTAT = 0.01;
	final static double GRAVITY_FORCE = 9.81;
	private double position;
	private double velocity;
	 
	public Cannonball(double ivel) { 
		position = 0;
		velocity = ivel;
	}
	
	public void move(double t){
		boolean start = true; 
		while (t > 0 && (position >= 0 || start)){
			if (start) start = false;
			position = position + velocity * DELTAT;
			velocity = velocity - DELTAT * GRAVITY_FORCE;
			t = t - DELTAT;
		}
	}	
	public double getVelocity() {
		return velocity;
	}
	public double getPosition() { 
		return position; 
	}
}

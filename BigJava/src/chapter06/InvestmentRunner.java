package chapter06;

public class InvestmentRunner {
	public static void main(String[] args){
		final double INITIAL_BALANCE = 10000;
		final double RATE = 5;
		final int YEARS = 20;
		Investment invest = new Investment(INITIAL_BALANCE, RATE);
		invest.waitForBalance(2 * INITIAL_BALANCE);
		int years = invest.getYears();
		System.out.printf("The investment doubled after " + years + " years\n");
		
		Investment invest2 = new Investment(INITIAL_BALANCE, RATE);
		invest2.waitYears(YEARS);
		double balance2 = invest2.getBalance();
		System.out.printf("The balance %d years is %.2f\n", YEARS, balance2);

		
	}
}

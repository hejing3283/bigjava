package chapter06;

public class CannonballTester {
	public static void main(String[] args){
		Cannonball ball = new Cannonball(100);
		for (int i = 1; i<=10; i++){
			ball.move(1);
			System.out.print("Simulation postion: "+ ball.getPosition() );
			System.out.println(" at velocity" + ball.getVelocity() );
			System.out.println("Exptected "  + (- 9.81 * 0.5 * i * i + 100 * i) );
			
		}
	}
}

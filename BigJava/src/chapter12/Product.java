package chapter12;

/**
Describes a product with a description and a price.
*/
public class Product
{
	private String description;
	private double price;
	public Product(){
		description = "";
		price = 0;
	}
	public Product(String aDescription, double aPrice){
		description = aDescription;
		price = aPrice;
	}
	/**
	Gets the product description.
	@return the description
	*/
	public String getDescription(){
		return description; 
	}
	/**
	Gets the product price.
	@return the unit price
	*/
	public double getPrice(){
		return price;
	}
}
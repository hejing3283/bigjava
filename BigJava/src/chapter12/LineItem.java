package chapter12;
/**
Describes a quantity of an article to purchase and its price.
*/
public class LineItem {
	private Product theProduct;
	private int quantity;
	public LineItem(){
		theProduct = null;
		quantity = 0;
	}
	public LineItem(Product aProduct){
		theProduct = aProduct;
		quantity = 1;
	}
	public LineItem(Product aProduct, int aQuantity){
		theProduct = aProduct;
		quantity = aQuantity;
	}
	/**
	Computes the total cost of this line item.
	@return the total price
	*/	
	public double getTotalPrice(){
		return theProduct.getPrice() * quantity;
	}
	/**
	Formats this item.
	@return a formatted string of this line item
	*/
	public String format(){
		return String.format("%-30s%8.2f%5d%8.2f",
				theProduct.getDescription(),
				theProduct.getPrice(),
				quantity, getTotalPrice());
	}
}

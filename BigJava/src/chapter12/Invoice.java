package chapter12;

import java.util.ArrayList;

/**
Describes an invoice for a set of purchased products.
*/
public class Invoice {
	private Address billingAddress;
	private ArrayList<LineItem> items;
	public Invoice(Address anAddress){
		billingAddress = anAddress;
		items = new ArrayList<LineItem>();
	}
	
	/**
	Adds a charge for a product to this invoice.
	@param aProduct the product that the customer ordered
	@param quantity the quantity of the product
	*/
	public void add(Product aProduct, int quantity){
		items.add(new LineItem(aProduct, quantity));
	}
	/**
	Formats the invoice.
	@return the formatted invoice
	*/
	public String format(){
		String r = "		  I N V O I C E\n\n "
				 + billingAddress.format() 
				 + String. format("\n\n%-30s%8s%5s%8s\n", 
						"Description", "Price", "Qty","Total");
		for ( LineItem i : items){
			r = r + i.format() + "\n";
		}
		r = r + String.format("\nAMOUT DUE: $%8.2f", getAmountDue());
		return r;
	}
	/**
	Describes a quantity of an article to purchase and its price.
	*/
	public double getAmountDue(){
		double amountDue = 0;
		for (LineItem i:items){
			amountDue = amountDue + i.getTotalPrice();
		}
		return amountDue;
	}
}


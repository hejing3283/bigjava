package chapter13;

public class Triangle {
	private int width; 
	public Triangle(int aWidth){
		width = aWidth;
	}
	
	public int getAera(){
		if(width <= 0) return 0;
		if(width == 1) return 1;
		Triangle smallerTriangle = new Triangle(width -1 );
		int smallerAera = smallerTriangle.getAera();
		return smallerAera + width;
	}

}

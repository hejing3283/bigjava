package chapter13;

public class TriangleTester {
	public static void main(String[] args){
		int w = 10;
		Triangle t = new Triangle(w);
		int aera = t.getAera();
		int expected = w * (w+1) / 2;
		System.out.println("Area checker: "+ (aera == expected));
	}

}

package chapter11;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.PrintWriter;
import java.util.Scanner;

import javax.swing.JFileChooser;

public class LIneNumberer {
	public static void main(String[] args)
						throws FileNotFoundException{
		
		Scanner console = new Scanner(System.in);
		System.out.print("Input file: ");
		String inputFileName = console.next();
		System.out.print("Output file: ");
		String outputFileName = console.next();
		Boolean useCommentDelimiters = false;
//		parsing the command line arguements
		for (String a: args){
			if (a.startsWith("-")){
				if(a.equals("-c")) useCommentDelimiters = true;
			}
			else if (inputFileName == null) inputFileName = a;
			else if (outputFileName == null) outputFileName = a;
		}
		
		
		FileReader reader = new FileReader(inputFileName);
		Scanner in = new Scanner(reader);
		PrintWriter out = new PrintWriter( outputFileName);
		int lineNumber = 1;
		
		while (in.hasNextLine()){
			String line = in.nextLine();
			out.println("/*" + lineNumber + "*/" + line);
			lineNumber++;
		}		
		in.close();
		out.close();
		System.out.println("END");
		
		
		// file chooser diaglogbox
		JFileChooser chooser = new JFileChooser();
		FileReader in2 = null;
		if(chooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION){
			File selectedFile = chooser.getSelectedFile();
			reader = new FileReader(selectedFile);
		}
		
		// catching exceptions
		
	}
}

package chapter11;

import chapter09.Measurer;

public class BankAccount implements Measurer {
	private int accountNumber;
	private double balance;
	private static int lastAssignedNumber = 1000; 
	public static final double OVERDRAFT_FEE  = 5;


	public BankAccount(){
		lastAssignedNumber++;
		accountNumber = lastAssignedNumber;
		balance = 0;
	}
	public BankAccount(double initialBalance){
		balance = initialBalance;
		lastAssignedNumber++;
		accountNumber = lastAssignedNumber;
	}
	
	public double measure(Object anObject){
		BankAccount account = (BankAccount) anObject;
		return account.getBalance();
	}
	public int getAccountNumber(){
		return accountNumber;
	}
	public void deposit(double amount){
		double newBalance = balance + amount;
		balance = newBalance; 
	}
	public void withdraw(double amount){
		if( balance < amount){ 
//			balance = balance - OVERDRAFT_FEE;
			throw new IllegalArgumentException("Amout exceeds balance");
		}else{
			double newBalance = balance - amount;
			balance = newBalance;
		} 
	}
	public double getBalance(){
		return balance;
	}
	public void addInterest(double rate){
		double newBalance = balance * rate + balance;
		balance = newBalance;
	}
	public void transfer(double amount, BankAccount other){
//		balance = balance - amount;
//		other.balance = other.balance + amount;	
		withdraw(amount);
		other.deposit(amount);
	}
	public String toString(){
		return getClass().getName() + "[balance=" 
				+ balance + "]";
	}
	
}

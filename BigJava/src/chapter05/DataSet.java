package chapter05;

public class DataSet {
	private static double n_min;
	private static double n_mid;
	private static double n_max;
	
	public DataSet(double num1, double num2, double num3){
		n_min = num1; 
		if ( num2 < n_min ){
			if ( num2 < num3 )
				n_min = num2;
			else
				n_min = num3;
		}else{
			if( num3 < n_min)
				n_min = num3; 
		}
		
		if ( n_min == num1 ){
			n_max = Math.max(num2, num3);
			n_mid = Math.min(num2, num3);
		}else if (n_min == num2){
			n_max = Math.max(num1, num3);
			n_mid = Math.min(num1, num3);	
		}else{
			n_max = Math.max(num1, num2);
			n_mid = Math.min(num1, num2);
		}
	}
	public double getSmallest(){
		return n_min;
	}
	public double getLargest(){
		return n_max;
	}
	public double getMiddle(){
		return n_mid;
	}

}

package chapter05;

public class Earthquake {
	private double richter;
	
	public Earthquake(double magnitude){
		richter = magnitude;	
	}
	
	public String getDescription(){
		String r;
		if(richter >= 8.0)
			r = "Most ";
		else if ( richter >=7)
			r = "Many ";
		else if ( richter >= 6.0)
			r = "Damage ";
		else if ( richter >= 4.5)
			r = "Felt ";
		else if ( richter >= 0)
			r = "Generally not ";
		else
			r = "Negative ";
		return r ;
	}
	
}


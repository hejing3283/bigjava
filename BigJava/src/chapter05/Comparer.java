package chapter05;

import java.util.logging.Logger;

public class Comparer {
	/**
	Creates a Comparer object to determine if two floating-point
	numbers are approximately the same.
	@param d the number of significant digits in the comparison 
	**/
	public int factor;
	public Comparer(int d)
	{
		factor = (int) Math.pow(10, d);
		Logger.global.info("number of decimal (d): " + factor);
	}
	public boolean areClose(double x1, double x2)
	{
		return (int) Math.floor(x1 * factor) == (int) Math.floor(x2 * factor);
	}
	public boolean roundToSame(double x1, double x2)
	{
		Logger.global.info("x1 " + x1 * factor);
		Logger.global.info("x2 " + x2 * factor);
		return (int) Math.round(x1 * factor) == (int) Math.round(x2 * factor);
	}
}

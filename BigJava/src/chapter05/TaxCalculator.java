package chapter05;

import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TaxCalculator {
	public static void main(String[] args){
//		Logger.global.setLevel(Level.FINE);
		Logger.global.setLevel(Level.OFF);
		Scanner in = new Scanner(System.in);
		
		System.out.print("Please enter your income: ");
		double income = in.nextDouble();
		
		System.out.print("Are you married? (Y/N)");
		String input = in.next();
		int status;
		if( input.equalsIgnoreCase("Y"))
			status = TaxReturn.MARRIED;
		else
			status = TaxReturn.SINGLE;
		TaxReturn aTaxReturn = new TaxReturn( income, status);
		
		System.out.println("Tax: " + aTaxReturn.getTax() + "\n" );
		in.close();
	}

}

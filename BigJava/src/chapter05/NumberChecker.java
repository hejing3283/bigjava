package chapter05;

import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class NumberChecker {
	public static void main(String[] args)
	{
		Logger.global.setLevel(Level.OFF);
		int dNun = 2; 
		Comparer c = new Comparer(dNun);
		Scanner in = new Scanner(System.in);
		System.out.println("Enter two floating-point numbers:");
		double x1 = in.nextDouble();
		double x2 = in.nextDouble();
		System.out.print("They are ");
		if (c.roundToSame(x1, x2))
			System.out.print("the same ");
		else
			System.out.print("different ");
		System.out.println("when rounded to %d decimal places.");
		System.out.print("They differ by ");
		if (c.areClose(x1, x2))
			System.out.print("less than ");
		else
			System.out.print("at least ");
		System.out.println(Math.pow(10, -1 * dNun)) ;
		
		in.close();
	}
}

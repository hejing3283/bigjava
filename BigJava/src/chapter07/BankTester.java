package chapter07;

import java.util.Scanner;

public class BankTester {
	public static void main(String[] args){
		Scanner in = new Scanner(System.in);
		
		Bank firstBankOfJava = new Bank();
		firstBankOfJava.addAccount(new BankAccount(1001,20000));
		firstBankOfJava.addAccount(new BankAccount(1015,10000));
		firstBankOfJava.addAccount(new BankAccount(1729,15000));
		
//		double threshold = 15000;
		double threshold = in.nextInt();
		
		int c = firstBankOfJava.count(threshold);
		int expected = in.nextInt();
		System.out.println("Count check: " + (c == expected));
		
//		int accountNumber = 1015;
		int accountNumber = in.nextInt();
		BankAccount a = firstBankOfJava.find(accountNumber);
		expected = in.nextInt(); 
		if(a == null)
			System.out.println("No matching account");
		else 
			System.out.println("Balance of matching account check; " 
					+ (a.getBalance() == expected ));
		
		expected = in.nextInt();
		BankAccount max = firstBankOfJava.getMaximum();
		System.out.println("Account with largest balance check: "
					+ (max.getAccountNumber() == expected) );
		
		in.close();
	}

}

package chapter07;

public class BankAccount {
	private int accountNumber;
	private double balance;

	public BankAccount(int anAccountNumber){
		accountNumber = anAccountNumber;
		balance = 0;
	}
	public BankAccount(int anAccountNumber, double initialBalance){
		balance = initialBalance;
		accountNumber = anAccountNumber;
	}
	
	public int getAccountNumber(){
		return accountNumber;
	}
	public void deposit(double amount){
		double newBalance = balance + amount;
		balance = newBalance; 
	}
	public void withdraw(double amount){
		double newBalance = balance - amount;
		balance = newBalance; 
	}
	public double getBalance(){
		return balance;
	}
	public void addInterest(double rate){
		double newBalance = balance * rate + balance;
		balance = newBalance;
	}
	
}

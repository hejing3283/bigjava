package chapter07;

public class TicTacToe {
	final static int ROWS = 10;
	final static int COLUMNS = 10;
	private String[][] board;
	
	public TicTacToe(){
		board = new String[ROWS][COLUMNS];
		for( int i = 0; i< ROWS; i++)
			for(int j = 0; j< COLUMNS; j++)
				board[i][j] =  " ";
	}
	public void set(int i, int j, String player){
		if (board[i][j].equals(" "))
			board[i][j]= player;
	}
	public String toString(){
		String r = "";
		for(int i = 0; i<ROWS; i++){
			r = r+"|";
			for(int j = 0; j<COLUMNS;j++)
				r = r+board[i][j];
			r = r+"|\n";
		}
		return r;
	}

}

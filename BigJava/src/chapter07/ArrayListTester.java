package chapter07;

import java.util.ArrayList;

public class ArrayListTester {
	
	public static void main(String[] args){
		ArrayList<BankAccount> accounts = new ArrayList< BankAccount>();
		accounts.add(new BankAccount(1001));
		accounts.add(new BankAccount(1015));
		accounts.add(new BankAccount(1729));
		accounts.add(1, new BankAccount(1008));
		accounts.remove(0);
		
		System.out.println("Size check " + (accounts.size() == 3));
		
		BankAccount first = accounts.get(0);
		System.out.println("First account check " 
				+ (first.getAccountNumber() == 1008));
		BankAccount last = accounts.get(accounts.size()-1);
		System.out.println("Last account check " 
				+ (last.getAccountNumber() == 1729));
	}
}

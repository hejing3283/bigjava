package chapter04;

public class CashRegisterTester {

	public static void main(String[] args){
		CashRegister register = new CashRegister();
		register.recordPurchase(100);
		register.recordPurchase(0);
		register.enterPayment(5,2,1,4,0);
		double change = register.giveChange();
		System.out.println(change == 11.25);
		System.out.println(register.playPurchase());
		
	}
	
}

package chapter04;

public class StampMachine {
	private double money;
	public static final double FIRST_CLASS_STAMP_PRICE = 0.39; 
	public StampMachine(){
		money = 0;
	}
	public void insert(double amount){
		money = money + amount;
	}
	public int giveFirstClassStamps(){
		int fisrtClasssStamps = (int) (money/ FIRST_CLASS_STAMP_PRICE);
		money = money - fisrtClasssStamps * FIRST_CLASS_STAMP_PRICE; 
		return fisrtClasssStamps;
	}
	public int givePennyStamps(){
		int pennyStamps = (int) Math.round(money * 100);
		money = 0;
		return pennyStamps;
	}
}

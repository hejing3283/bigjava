package chapter04;

import static org.junit.Assert.*;
import org.junit.Test;

import junit.framework.Assert;
public class CashRegisterTest{
	private static final double EPSILON = 1E-12;
	@Test
	public void testSimpleCase()
	{
		CashRegister register = new CashRegister();
		register.recordPurchase(0.75);
		register.recordPurchase(1.50);
		register.enterPayment(2, 0, 5, 0, 0);
		double expected = 0.25;
		assertEquals(expected, register.giveChange(), EPSILON);
	}

}

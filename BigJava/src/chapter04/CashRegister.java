package chapter04;

public class CashRegister {
	public CashRegister(){
		purchase = 0;
		payment = 0; 
	}
	public void recordPurchase(double amount){
		double newPurchase = purchase + amount;
		purchase = newPurchase; 
	}
	public void enterPayment(int dollars, int quarters, int dimes, int nickels, int pennies){
		double newPayment = payment + dollars  +  quarters * QUARTER_VALUE + dimes * DIM_VALUE 
				+ nickels * NICKEL_VALUE + pennies * PENNY_VALUE;
		payment = newPayment;
	}
	public double giveChange(){
		return payment - purchase;
	}
	
	public double playPurchase(){
//		return Math.pow(purchase, 1.1);
		return Math.sqrt(purchase);

	}
	public static final double QUARTER_VALUE = 0.25;
	public static final double DIM_VALUE = 0.1;
	public static final double NICKEL_VALUE = 0.05;
	public static final double PENNY_VALUE = 0.01;
	
	private double purchase = 0;
	private double payment = 0;  
//	private double change = 0;  

}

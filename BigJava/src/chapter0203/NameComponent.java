package chapter0203;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;

import javax.swing.JComponent;


public class NameComponent extends JComponent{
	public void paintComponent(Graphics g){
		Graphics2D g2 = (Graphics2D) g;
		
		Rectangle box = new Rectangle(5,10,300,100);
		g2.setColor(Color.cyan);
		g2.fill(box);
		
		g2.setColor(Color.RED);
		g2.drawString("Jing He" , 10, 100);
			
	}
}

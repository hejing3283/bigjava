package chapter0203;
import java.awt.Rectangle;


public class Rectangle_Tester {
	public static void main(String[] args){
		
		Rectangle box = new Rectangle(5, 10, 20, 30);
		
		box.translate(15,25);
		
		System.out.print("Test status: X:  ");
		System.out.println(box.x == 20);
		System.out.print("Test status: Y:  ");
		System.out.println(box.y == 35);
		
		
	}
}

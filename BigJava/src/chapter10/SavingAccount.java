package chapter10;

import chapter09.BankAccount;

public class SavingAccount extends BankAccount {
	private double interestRate;
	
	public SavingAccount(double rate){
		interestRate = rate;
	}
	
	public void addInterest(){
		double interest = getBalance() * interestRate/100;
		deposit(interest);
	}
	public String toString(){
		return super.toString() + "interestRate=" + interestRate + "]";
	}
	
}

package chapter10;

public class AccountTester {
	public static void main(String[] args){
		SavingAccount momsSavings 
			= new SavingAccount(0.5);
		CheckingAccount harrysChecking 
			= new CheckingAccount(100);
		momsSavings.deposit(10000);
		momsSavings.transfer(2000, harrysChecking);
		harrysChecking.withdraw(1500);
		harrysChecking.withdraw(80);
		
		momsSavings.transfer(1000, harrysChecking);
		harrysChecking.withdraw(400);
		
		momsSavings.addInterest();
		harrysChecking.deductFees();
		double expected = 7035; 
		System.out.println("Mom's savings balance "+
				(momsSavings.getBalance() == expected));
		expected = 1116; 
		System.out.println("Harry's savings balance "+
				(harrysChecking.getBalance() == expected));
		
	}

}

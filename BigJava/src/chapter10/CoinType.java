package chapter10;

public enum CoinType {
	PENNY(0.01), NICKEL(0.05), DIME(0.1), QUARTER(0.25), DOLLAR(1);
	private double value;
	
	CoinType(double aValue){value = aValue;}
	
	public double getValue(){return value;}
	
	
}

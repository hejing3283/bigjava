package chapter09;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;

import javax.swing.JComponent;

public class RectangleComponent extends JComponent{
	private final static int BOX_X = 10;
	private final static int BOX_Y = 20;
	private final static int BOX_WIDTH = 20;
	private final static int BOX_HEIGHT = 30;
	private Rectangle box;
	
	public RectangleComponent(){
		box = new Rectangle(BOX_X, BOX_Y, BOX_WIDTH, BOX_HEIGHT);
	}
	
	public void paintComponent(Graphics g){
		super.paintComponent(g); // ?
		Graphics2D g2 = (Graphics2D) g;
		g2.draw(box);	
	}
	public void moveBy(int dx, int dy){
		box.translate(dx, dy);
		repaint();
	}
	public void moveTo(int x, int y){
		box.setLocation(x, y);
		repaint();
	}
}

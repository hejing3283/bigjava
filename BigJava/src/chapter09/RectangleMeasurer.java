package chapter09;

import java.awt.Rectangle;

public class RectangleMeasurer implements Measurer{
	public double measure(Object anObject){
		Rectangle aRectangle = (Rectangle) anObject;
		double aera = aRectangle.getWidth() * aRectangle.getHeight();
		return aera;
	}
}

package chapter09;

public interface Measurable {
	double getMeasure();
}

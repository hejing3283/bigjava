package chapter09;

import java.awt.Rectangle;


public class DataSetTester {
	public static void main(String[] args){
		Measurer m = new RectangleMeasurer();
		
		DataSet data = new DataSet(m);
		data.add(new Rectangle(5,10,20,30));
		data.add(new Rectangle(10,20,30,40));
		data.add(new Rectangle(20,30,5,15));
		double expected = 625;
		System.out.println( "Average Area: " + (data.getAverage() == expected));
		Rectangle max = (Rectangle) data.getMaximum();
		Rectangle expectedMax = new Rectangle(10,20,30,40);
		System.out.println( "Average Area: " + max + "  " + expectedMax);
		
	}

}

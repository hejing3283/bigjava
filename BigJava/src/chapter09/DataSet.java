package chapter09;

/**
Computes information about a set of data values. 
Modified for bank account. 
*/
public class DataSet implements Measurer {
	
	private double sum;
	private Object maximum;
	private Measurer measurer;
	private int count;
	
	public DataSet(Measurer aMeasurer){
		sum = 0;
		count = 0;
		maximum = null;
		measurer = aMeasurer;
	}
	
	public double measure(Object anObject) {
		return sum;
	}
	public double getMeasure(){
		return sum; 
	}
	public void add(Object x){
		sum = sum +  measurer.measure(x);
		if (count == 0|| 
				(measurer.measure(maximum) < measurer.measure(x))) 
			maximum = x;
		count++;
	}
	public Object getMaximum(){
		return maximum;
	}
	public double getAverage(){
		double avg = 0;
		if(count > 0 ) 
			avg = sum / count;
		return avg;
	}

}

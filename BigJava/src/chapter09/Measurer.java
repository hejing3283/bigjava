package chapter09;

public interface Measurer {
	double measure(Object anObject);
}

package chapter09;

import java.awt.event.ActionEvent;

public interface ActionListener {
	void actionPerformed(ActionEvent event);
}

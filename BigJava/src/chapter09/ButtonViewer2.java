package chapter09;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;

public class ButtonViewer2 {
	
	private static final int FRAME_WIDTH = 300;
	private static final int FRAME_HEIGHT = 400;
	
	public static void main(String[] args){
		
		// using inner class 
		class MyListener implements ActionListener {
				public void actionPerformed(ActionEvent event ){
					System.out.println("Inner Class CLicked!");
				}
		};
		
		// other
		JFrame frame = new JFrame();
		JButton button = new JButton("An Inner CLass Listener Click HERE!");
		frame.add(button);

		ActionListener listener = new MyListener();
		button.addActionListener(listener);
		
		frame.setSize(FRAME_WIDTH, FRAME_HEIGHT);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}

}

package chapter09;

public class Coin implements Measurable{
	private double value;
	private String name;
	
	public Coin(double aValue, String aName){
		value = aValue;
		name = aName;
	}
	
	public double getMeasure(){
		return value;
	}
}

package chapter09;

public class BankAccount implements Measurable {
	private int accountNumber;
	private double balance;
	private static int lastAssignedNumber = 1000; 
	public static final double OVERDRAFT_FEE  = 5;


	public BankAccount(){
		lastAssignedNumber++;
		accountNumber = lastAssignedNumber;
		balance = 0;
	}
	public BankAccount(double initialBalance){
		balance = initialBalance;
		lastAssignedNumber++;
		accountNumber = lastAssignedNumber;
	}
	
	public double getMeasure(){
		return balance;
	}
	public int getAccountNumber(){
		return accountNumber;
	}
	public void deposit(double amount){
		double newBalance = balance + amount;
		balance = newBalance; 
	}
	public void withdraw(double amount){
		if( balance < amount) balance = balance - OVERDRAFT_FEE;
		else{
			double newBalance = balance - amount;
			balance = newBalance;
		} 
	}
	public double getBalance(){
		return balance;
	}
	public void addInterest(double rate){
		double newBalance = balance * rate + balance;
		balance = newBalance;
	}
	public void transfer(double amount, BankAccount other){
//		balance = balance - amount;
//		other.balance = other.balance + amount;	
		withdraw(amount);
		other.deposit(amount);
	}
	
	
}

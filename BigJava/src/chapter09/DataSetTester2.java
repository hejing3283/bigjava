package chapter09;

import java.awt.Rectangle;

public class DataSetTester2 {
	public static void main(String[] args){
		class RectangleMeasurer implements Measurer {	
			public double measure(Object anObject){
				Rectangle aRectangle = (Rectangle) anObject;
				double aera = 
					aRectangle.getWidth() * aRectangle.getHeight();
				return aera;
			}
		}
		Measurer m = new RectangleMeasurer();
		DataSet data = new DataSet(m);
		data.add(new Rectangle(5,10,20,30));
		data.add(new Rectangle(10,30,30,40));
		data.add(new Rectangle(20,30,5,15));
		double expected = 625;
		System.out.println( "Average Area: " + (data.getAverage() == expected));
		Rectangle max = (Rectangle) data.getMaximum();
		Rectangle expectedMax = new Rectangle(10,30,30,40);
		System.out.println( "Average Area: " + max + "  " + expectedMax);
	}
}

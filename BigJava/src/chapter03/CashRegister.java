package chapter03;

public class CashRegister {
	public CashRegister(){
		purchase = 0;
		payment = 0; 
	}
	public void recordPurchase(double amount){
		double newPurchase = purchase + amount;
		purchase = newPurchase; 
	}
	public void enterPayment(double amount){
		double newPayment = payment + amount ;
		payment = newPayment;
	}
	public double giveChange(){
		return payment - purchase;
	}
	private double purchase = 0;
	private double payment = 0;  
//	private double change = 0;  

}

package chapter03;

public class BankAccountTester {
	public static void main(String args){
	BankAccount heAccount = new BankAccount();
	BankAccount jingAccount = new BankAccount(1000.1);
	
	heAccount.deposit(100.0);
	jingAccount.withdraw(50.0);
	System.out.println("Test1");
	System.out.println( heAccount.getBalance() == 50 ) ;
	
	BankAccount momsSavings = new BankAccount(1000);
	momsSavings.addInterest(10);
	System.out.println("Test2");
	System.out.println(momsSavings.getBalance() == 1100);

	}
}

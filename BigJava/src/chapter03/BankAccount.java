package chapter03;

public class BankAccount {
	public BankAccount(){
		balance = 0;
	}
	public BankAccount(double initialBalance){
		balance = initialBalance;
	}
	
	public void deposit(double amount){
		double newBalance = balance + amount;
		balance = newBalance; 
	}

	public void withdraw(double amount){
		double newBalance = balance - amount;
		balance = newBalance; 
	}
	public double getBalance(){
		return balance;
	}
	public void addInterest(double rate){
		double newBalance = balance * rate + balance;
		balance = newBalance;
	}
	
	private double balance;
}
